FROM debian:buster-slim

LABEL description="SinusBot - Discord only image"
LABEL version="1.0.0-beta.14-dc94a7c"

# Install dependencies and clean up afterwards
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends ca-certificates bzip2 unzip curl \
    python procps libpci3 libxslt1.1 libxkbcommon0 locales x11vnc xvfb libxcursor1 libnss3 libegl1-mesa \
    libasound2 libglib2.0-0 libxcomposite-dev less jq wget && \
    rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/*


# Set locale
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && locale-gen
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en

WORKDIR /sinusbot

ADD sinusbot/install.sh .
RUN chmod 755 install.sh

ADD sinusbot/entrypoint.sh .
RUN chmod 755 entrypoint.sh

ADD sinusbot/install-sinusbot.sh .
RUN chmod 755 install-sinusbot.sh

RUN bash install.sh sinusbot
RUN bash install.sh youtube-dl
RUN bash install.sh text-to-speech
RUN bash install.sh teamspeak

RUN chown -R 997:997 /sinusbot


EXPOSE 8087
