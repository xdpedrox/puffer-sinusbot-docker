#!/bin/bash

# print kernel/system info
cd /sinusbot

if [ ! -f "/pufferpanel/config.ini" ]; then
  cp config.ini.configured /pufferpanel/config.ini
fi
ln -fs /pufferpanel/config.ini /sinusbot/config.ini


rm -rf /sinusbot/data
rm -rf /sinusbot/scripts

ln -fs /pufferpanel/data /sinusbot/data
ln -fs /pufferpanel/scripts /sinusbot/scripts  

SINUSBOT="/sinusbot/sinusbot"
YTDL="youtube-dl"

echo "Updating youtube-dl..."
$YTDL --restrict-filename -U
$YTDL --version

PID=0

# graceful shutdown on kill
kill_handler() {
  echo "Shutting down..."
  kill -s SIGINT -$(ps -o pgid= $PID | grep -o '[0-9]*')
  while [ -e /proc/$PID ]; do
    sleep .5
  done
  exit 0
}

trap 'kill ${!}; kill_handler' SIGTERM # docker stop
trap 'kill ${!}; kill_handler' SIGINT  # CTRL + C

echo "Clearing youtube-dl cache..."
$YTDL --rm-cache-dir

echo $SINUSBOT

echo "Starting SinusBot..."
if [[ -v OVERRIDE_PASSWORD ]]; then
  echo "Overriding password..."
  $SINUSBOT --override-password="${OVERRIDE_PASSWORD}" &
else
  $SINUSBOT &
fi

PID=$!
echo "PID: $PID"

while true; do
  tail -f /dev/null & wait ${!}
done
