#!/bin/bash
cd /sinusbot

# Preparing folders for symlink
echo '============================='
echo 'Preparing folders for symlink'
echo '============================='

# Creating folders
mkdir /pufferpanel
mkdir /pufferpanel/data
mkdir /pufferpanel/scripts

# Moving files from sinusbot to the new folders
mv /sinusbot/data/* /pufferpanel/data
mv /sinusbot/scripts/* /pufferpanel/scripts

# Removing sinusbot folders
rm -rf /sinusbot/data
rm -rf /sinusbot/scripts

echo '============================='
echo 'Done'
echo '============================='
